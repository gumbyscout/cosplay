describe("projects", () => {
  beforeEach(() => {
    cy.visit("/projects");
    cy.get("[data-cy=project-item]").as("items");
  });

  it.skip("creates, edits, and deletes a project", () => {
    const projectName = "ZED";

    cy.get("@items").should("have.length", 3);

    cy.get("[data-cy=create-project-btn]").click();

    cy.get("[data-cy=project-name]")
      .as("project-name")
      .should("have.value", "New Project");

    cy.get("@project-name")
      .clear()
      .type(projectName, { delay: 100 })
      .blur();

    cy.visit("/projects");

    cy.get("@items")
      .should("have.length", 4)
      .contains(projectName)
      .click();

    cy.get("@project-name").should("have.value", projectName);

    cy.get("[data-cy=delete-project-btn]").click();

    cy.get("@items")
      .should("have.length", 3)
      .should("not.contain", projectName);
  });
});
