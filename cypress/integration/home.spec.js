// @flow
describe("home", () => {
  it("loads succesfully", () => {
    cy.visit("/");
  });

  it("has the app header", () => {
    cy.get("[data-cy=header]");
  });

  it("has the nav drawer", () => {
    cy.get("[data-cy=drawer]");
  });
});
