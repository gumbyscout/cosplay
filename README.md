# Cosplay
A simple app to manage cosplay projects.

All ideas, todo, and inprogress tickets are tracked in
[Taiga](https://tree.taiga.io/project/gumbyscout-cosplay/)

## Development
- Install dependencies
```
$ yarn
```
- Run dev application
```
$ yarn dev
```
