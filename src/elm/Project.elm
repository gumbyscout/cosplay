module Project exposing (Project, ProjectID, decode, encode, remainingTimeInDays, setName)

import Dict exposing (Dict)
import Image exposing (Image)
import Json.Decode as D exposing (Decoder, Value)
import Json.Decode.Pipeline exposing (hardcoded, optional, required)
import Json.Encode as E exposing (null)
import RemoteData exposing (RemoteData)
import Time exposing (Posix)
import Time.Extra as Time


type alias ProjectID =
    String


type alias Project =
    { id : ProjectID
    , accountID : Maybe String
    , name : String
    , description : String
    , createdDate : Posix
    , targetDate : Maybe Posix
    , media : List Image
    , completed : Bool
    , meta : Value
    }


setName : Project -> String -> Project
setName project name =
    { project | name = name }


remainingTimeInDays : Project -> Maybe Int
remainingTimeInDays project =
    case project.targetDate of
        Just posix ->
            Just (Time.diff Time.Day Time.utc project.createdDate posix)

        Nothing ->
            Nothing


decodePosix : Decoder Posix
decodePosix =
    D.map Time.millisToPosix D.int


encodePosix : Maybe Posix -> E.Value
encodePosix p =
    Maybe.map Time.posixToMillis p
        |> Maybe.withDefault 0
        |> E.int


decode : Decoder Project
decode =
    D.succeed Project
        |> required "id" D.string
        |> hardcoded Nothing
        |> required "name" D.string
        |> optional "description" D.string ""
        |> required "createdDate" decodePosix
        |> optional "targetDate" (D.maybe decodePosix) Nothing
        |> optional "media" (D.list Image.decode) []
        |> optional "completed" D.bool False
        |> optional "meta" D.value null


encode : Project -> E.Value
encode project =
    E.object
        -- TODO: Add encoders for media
        [ ( "id", E.string project.id )
        , ( "name", E.string project.name )
        , ( "createdDate", encodePosix <| Just project.createdDate )
        , ( "targetDate", encodePosix project.targetDate )
        , ( "meta", project.meta )
        ]
