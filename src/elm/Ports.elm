port module Ports exposing (fromStorage, toStorage)

import Json.Encode exposing (Value)


port toStorage : Value -> Cmd msg


port fromStorage : (Value -> msg) -> Sub msg
