module Layout exposing (view)

import Browser exposing (Document)
import Components.Icon as Icon
import Element
    exposing
        ( Element
        , column
        , el
        , fill
        , height
        , image
        , inFront
        , layout
        , link
        , padding
        , paddingEach
        , px
        , rgba
        , row
        , scrollbarY
        , spacing
        , text
        , width
        )
import Element.Background as Background
import Element.Font as Font
import Element.Input exposing (button)
import Element.Region as Region
import Model exposing (Model, getDevice)
import Page exposing (Page)
import Route exposing (toUrl)
import Theme exposing (color, halfRhythm, rhythm, scaled)
import Update exposing (Msg(..))
import Utils exposing (cyAttribute)


drawer : Element msg
drawer =
    column
        [ height fill
        , Background.color color.material
        , Region.navigation
        , cyAttribute "drawer"
        ]
        [ link [ padding rhythm, width fill ]
            { url = toUrl Route.Projects
            , label = row [ spacing halfRhythm ] [ Icon.view "cut", text "Projects" ]
            }
        , link [ padding rhythm, width fill ]
            { url = toUrl Route.Statistics
            , label = row [ spacing halfRhythm ] [ Icon.view "chart-bar", text "Statistics" ]
            }
        , link [ padding rhythm, width fill ]
            { url = toUrl Route.AppSettings
            , label = row [ spacing halfRhythm ] [ Icon.view "cog", text "Settings" ]
            }
        ]


header : Bool -> Page msg -> (msg -> Msg) -> Element Msg
header isPhone page toMsg =
    let
        ( headingPadding, hamburger ) =
            if isPhone then
                ( paddingEach { top = rhythm, bottom = rhythm, left = 0, right = rhythm }
                , button [ padding rhythm ]
                    { label = Icon.view "bars", onPress = Just ToggleDrawer }
                )

            else
                ( padding rhythm, Element.none )
    in
    row
        [ width fill
        , Background.color color.primary
        , Font.color color.primaryText
        , Region.heading 1
        , cyAttribute "header"
        ]
        [ row
            [ Font.size (scaled 3), width fill ]
            (hamburger :: [ el [ headingPadding, width fill ] (Element.map toMsg page.heading) ])
        , row [] <|
            List.map
                (\( icon, cyAttr, onPress ) ->
                    Element.map toMsg <|
                        button
                            [ padding rhythm, cyAttribute cyAttr ]
                            { label = Icon.view icon, onPress = onPress }
                )
                page.actions
        ]


contentWrapper : Element msg -> Element msg
contentWrapper content =
    column [ width fill, height fill, Region.mainContent, scrollbarY ] [ content ]


other : Model -> Page msg -> (msg -> Msg) -> Element Msg
other model page toMsg =
    row [ height fill, width fill ]
        [ drawer
        , column [ height fill, width fill ]
            [ header False page toMsg
            , Element.map toMsg <| contentWrapper page.content
            ]
        ]


openedDrawer : Element Msg
openedDrawer =
    row [ height fill, width fill ]
        [ drawer
        , button [ height fill, width fill, Background.color (rgba 0 0 0 0.5) ]
            { label = Element.none
            , onPress = Just ToggleDrawer
            }
        ]


phone : Model -> Page msg -> (msg -> Msg) -> Element Msg
phone model page toMsg =
    let
        drawerEl =
            if model.drawerIsOpen then
                openedDrawer

            else
                Element.none
    in
    column
        [ width fill
        , height fill
        , inFront drawerEl
        ]
        [ header True page toMsg
        , Element.map toMsg <| contentWrapper page.content
        ]


view : Model -> Page msg -> (msg -> Msg) -> Document Msg
view model page toMsg =
    let
        device =
            getDevice model
    in
    { title = page.title
    , body =
        [ layout
            [ Background.color color.background
            , Font.size rhythm
            , Font.family
                [ Font.external
                    { name = "Roboto"
                    , url = "https://fonts.googleapis.com/css?family=Roboto"
                    }
                , Font.sansSerif
                ]
            , height fill
            ]
          <|
            case device.class of
                Element.Phone ->
                    phone model page toMsg

                _ ->
                    other model page toMsg
        ]
    }
