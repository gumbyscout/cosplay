module Utils exposing (cyAttribute, monthToNumString)

import Element exposing (Attribute, htmlAttribute)
import Html.Attributes exposing (attribute)
import Time exposing (Month(..))


cyAttribute : String -> Attribute msg
cyAttribute =
    htmlAttribute << attribute "data-cy"


monthToNumString : Month -> String
monthToNumString month =
    case month of
        Jan ->
            "01"

        Feb ->
            "02"

        Mar ->
            "03"

        Apr ->
            "04"

        May ->
            "05"

        Jun ->
            "06"

        Jul ->
            "07"

        Aug ->
            "08"

        Sep ->
            "09"

        Oct ->
            "10"

        Nov ->
            "11"

        Dec ->
            "12"
