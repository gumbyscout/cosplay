module Storage exposing
    ( Action(..)
    , Data
    , createProject
    , createTask
    , decodeAction
    , deleteProject
    , getProject
    , getProjects
    , getTasks
    , getType
    , gotAction
    , projectFromAction
    , projectsFromAction
    , saveProject
    , saveTask
    , tasksFromAction
    )

import Json.Decode as D
import Json.Encode as E
import Ports
import Project exposing (Project, ProjectID)
import ProjectTask exposing (Task)
import RemoteData exposing (RemoteData(..))


type Action
    = GetProjects
    | GotProjects (List Project)
    | GetProject ProjectID
    | GotProject Project
    | CreateProject
    | CreatedProject ProjectID
    | SaveProject Project
    | SavedProject Identifier
    | GetTasks ProjectID
    | GotTasks (List Task)
    | SaveTask Task
    | DeleteProject Project
    | DeletedProject ProjectID
    | CreateTask ProjectID
    | CreatedTask Task


type alias Data a =
    RemoteData D.Error a


getType : Action -> String
getType a =
    case a of
        GetProjects ->
            "GET_PROJECTS"

        GotProjects _ ->
            "GOT_PROJECTS"

        GetProject _ ->
            "GET_PROJECT"

        GotProject _ ->
            "GOT_PROJECT"

        CreateProject ->
            "CREATE_PROJECT"

        CreatedProject _ ->
            "CREATED_PROJECT"

        SaveProject _ ->
            "SAVE_PROJECT"

        SavedProject _ ->
            "SAVED_PROJECT"

        GotTasks _ ->
            "GOT_TASKS"

        GetTasks _ ->
            "GET_TASKS"

        SaveTask _ ->
            "SAVE_TASK"

        DeleteProject _ ->
            "DELETE_PROJECT"

        DeletedProject _ ->
            "DELETED_PROJECT"

        CreateTask _ ->
            "CREATE_TASK"

        CreatedTask _ ->
            "CREATED_TASK"


basicAction : Action -> Cmd msg
basicAction action =
    Ports.toStorage <|
        E.object
            [ ( "type", E.string <| getType action ) ]


getProjects : Cmd msg
getProjects =
    basicAction GetProjects


getProject : ProjectID -> Cmd msg
getProject id =
    Ports.toStorage <|
        E.object
            [ ( "type", E.string <| getType <| GetProject id )
            , ( "data", E.string id )
            ]


createProject : Cmd msg
createProject =
    basicAction CreateProject


projectsFromAction : Action -> Data (List Project)
projectsFromAction a =
    case a of
        GotProjects projects ->
            RemoteData.Success projects

        _ ->
            RemoteData.NotAsked


projectFromAction : Action -> Data Project
projectFromAction a =
    case a of
        GotProject project ->
            RemoteData.Success project

        _ ->
            RemoteData.NotAsked


tasksFromAction : Action -> Data (List Task)
tasksFromAction a =
    case a of
        GotTasks tasks ->
            RemoteData.Success tasks

        _ ->
            RemoteData.NotAsked


getTasks : ProjectID -> Cmd msg
getTasks projectID =
    Ports.toStorage <|
        E.object
            [ ( "type", E.string <| getType <| GetTasks projectID )
            , ( "data", E.string projectID )
            ]


saveTask : Task -> Cmd msg
saveTask task =
    Ports.toStorage <|
        E.object
            [ ( "type", E.string <| getType <| SaveTask task )
            , ( "data", ProjectTask.encode task )
            ]


deleteProject : Project -> Cmd msg
deleteProject project =
    Ports.toStorage <|
        E.object
            [ ( "type", E.string <| getType <| DeleteProject project )
            , ( "data", Project.encode project )
            ]


saveProject : Project -> Cmd msg
saveProject project =
    Ports.toStorage <|
        E.object
            [ ( "type", E.string <| getType <| SaveProject project )
            , ( "data", Project.encode project )
            ]


createTask : ProjectID -> Cmd msg
createTask projectID =
    Ports.toStorage <|
        E.object
            [ ( "type", E.string <| getType <| CreateTask projectID )
            , ( "data", E.string projectID )
            ]


gotAction : (Data Action -> msg) -> Sub msg
gotAction handler =
    Ports.fromStorage (handler << decodeAction)


decodeData : (a -> Action) -> D.Decoder a -> D.Value -> Data Action
decodeData a d v =
    D.decodeValue (D.field "data" d) v
        |> RemoteData.fromResult
        |> RemoteData.map a


type alias Identifier =
    { id : String, meta : E.Value }


decodeIdentifier : D.Decoder Identifier
decodeIdentifier =
    D.map2 Identifier
        (D.field "id" D.string)
        (D.field "meta" D.value)


decodeAction : E.Value -> Data Action
decodeAction a =
    let
        decoded =
            D.decodeValue (D.field "type" D.string) a
    in
    case decoded of
        Ok "GOT_PROJECTS" ->
            decodeData GotProjects (D.list Project.decode) a

        Ok "GOT_PROJECT" ->
            decodeData GotProject Project.decode a

        Ok "GOT_TASKS" ->
            decodeData GotTasks (D.list ProjectTask.decode) a

        Ok "CREATED_PROJECT" ->
            decodeData CreatedProject D.string a

        Ok "DELETED_PROJECT" ->
            decodeData DeletedProject D.string a

        Ok "SAVED_PROJECT" ->
            decodeData SavedProject decodeIdentifier a

        Ok "CREATED_TASK" ->
            decodeData CreatedTask ProjectTask.decode a

        _ ->
            RemoteData.NotAsked
