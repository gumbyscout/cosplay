module ProjectTask exposing (Task, TaskID, decode, emptyTask, encode, getTask, setDescription, setName, toggleIsDone)

import Dict exposing (Dict)
import Json.Decode as D exposing (Decoder, Value)
import Json.Decode.Pipeline exposing (hardcoded, optional, required)
import Json.Encode as E exposing (null)
import Project exposing (ProjectID)
import RemoteData


type alias TaskID =
    String


type alias Task =
    { id : TaskID
    , projectID : ProjectID
    , name : String
    , isDone : Bool
    , description : String
    , meta : E.Value
    }


emptyTask : Task
emptyTask =
    { id = ""
    , projectID = ""
    , name = ""
    , isDone = False
    , description = ""
    , meta = null
    }


decode : Decoder Task
decode =
    D.succeed Task
        |> required "id" D.string
        |> required "projectID" D.string
        |> required "name" D.string
        |> required "isDone" D.bool
        |> required "description" D.string
        |> required "meta" D.value


encode : Task -> E.Value
encode task =
    E.object
        [ ( "id", E.string task.id )
        , ( "projectID", E.string task.projectID )
        , ( "name", E.string task.name )
        , ( "isDone", E.bool task.isDone )
        , ( "description", E.string task.description )
        , ( "meta", task.meta )
        ]


getTask : List Task -> TaskID -> Maybe Task
getTask tasks taskID =
    tasks
        |> List.filter (\t -> t.id == taskID)
        |> List.head


toggleIsDone : Task -> Task
toggleIsDone task =
    { task | isDone = not task.isDone }


setName : String -> Task -> Task
setName name task =
    { task | name = name }


setDescription : String -> Task -> Task
setDescription description task =
    { task | description = description }
