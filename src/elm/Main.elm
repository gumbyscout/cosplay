module Main exposing (main)

import Browser exposing (Document, application)
import Browser.Events as Events
import Browser.Navigation as Nav
import Flags
import Html
import Json.Decode as D
import Json.Encode exposing (Value)
import Layout exposing (view)
import Model exposing (Model, initial)
import Page exposing (Page)
import Page.NotFound as NotFound
import Page.Project as Project
import Page.Projects as Projects
import Route exposing (Route)
import Update exposing (Msg(..), changeRouteTo, update)
import Url exposing (Url)


init : Value -> Url -> Nav.Key -> ( Model, Cmd Msg )
init f url key =
    let
        flags =
            D.decodeValue Flags.decode f

        window =
            flags
                |> Result.map .window
                |> Result.withDefault { height = 0, width = 0 }

        projects =
            flags
                |> Result.map .projects
                |> Result.withDefault []

        model =
            initial key window projects
    in
    changeRouteTo (Route.fromUrl url) model


viewPage : (a -> Msg) -> Page a -> Model -> Document Msg
viewPage toMsg subView model =
    Layout.view model subView toMsg


view : Model -> Document Msg
view model =
    case model.route of
        Route.Projects ->
            viewPage GotProjectsMsg (Projects.view model.projectsModel) model

        Route.Project subRoute projectId ->
            viewPage GotProjectMsg (Project.view model.projectModel) model

        _ ->
            viewPage identity NotFound.view model


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Events.onResize WindowResized
        , Sub.map GotProjectsMsg (Projects.subscriptions model.projectsModel)
        , Sub.map GotProjectMsg (Project.subscriptions model.projectModel)
        ]


main : Program Value Model Msg
main =
    application
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = ClickedLink
        , onUrlChange = ChangedUrl
        }
