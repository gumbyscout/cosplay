module Image exposing (Image, decode, getThumbnail)

import Json.Decode as D exposing (Decoder, Value)
import Json.Decode.Pipeline exposing (hardcoded, optional, required)


type alias Image =
    { name : String
    , url : String
    }


decode : Decoder Image
decode =
    D.succeed Image
        |> required "name" D.string
        |> required "url" D.string


getThumbnail : List Image -> String
getThumbnail images =
    let
        thumbnail =
            List.filter (\i -> i.name == "thumbnail") images
                |> List.head
    in
    case thumbnail of
        Nothing ->
            ""

        Just t ->
            t.url
