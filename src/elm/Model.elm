module Model exposing (Model, closeDrawer, getDevice, initial, updateProjectModel, updateProjectsModel)

import Browser.Navigation as Nav
import Element
import Page.Project as Project
import Page.Project.Model as ProjectModel
import Page.Projects.Model as ProjectsModel
import Project exposing (Project)
import Route exposing (Route)


type alias Model =
    { navKey : Nav.Key
    , window : Window
    , drawerIsOpen : Bool
    , session : Session
    , route : Route
    , projectModel : ProjectModel.Model
    , projectsModel : ProjectsModel.Model
    }


initial : Nav.Key -> Window -> List Project -> Model
initial navKey window projects =
    { navKey = navKey
    , window = window
    , drawerIsOpen = False
    , session = Guest
    , route = Route.Projects
    , projectModel = ProjectModel.initial
    , projectsModel = ProjectsModel.initial projects
    }


type alias Window =
    { width : Int, height : Int }


type Session
    = LoggedIn
    | Guest


updateProjectsModel : Model -> ProjectsModel.Model -> Model
updateProjectsModel model =
    \m -> { model | projectsModel = m }


updateProjectModel : Model -> ProjectModel.Model -> Model
updateProjectModel model =
    \m -> { model | projectModel = m }


closeDrawer : Model -> Model
closeDrawer model =
    { model | drawerIsOpen = False }


getDevice : Model -> Element.Device
getDevice model =
    .window model
        |> Element.classifyDevice
