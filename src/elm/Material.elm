module Material exposing (Material, MaterialID, calcBudget, calcCost, calcCurrentCost, setCost, setName, toggleIsAcquired)

import Dict
import Json.Encode exposing (Value, null)
import ProjectTask exposing (TaskID)
import RemoteData


type alias MaterialID =
    String


type alias Material =
    { id : MaterialID
    , taskID : TaskID
    , name : String
    , cost : Float
    , isAcquired : Bool
    , meta : Value
    }


emptyMaterial : Material
emptyMaterial =
    { id = ""
    , taskID = ""
    , name = ""
    , cost = 0
    , isAcquired = False
    , meta = null
    }


calcCost : List Material -> Float
calcCost =
    List.foldr (\mat total -> mat.cost + total) 0


calcBudget : List Material -> String
calcBudget materials =
    calcCost materials
        |> String.fromFloat


calcCurrentCost : List Material -> String
calcCurrentCost materials =
    List.filter (\mat -> mat.isAcquired) materials
        |> calcCost
        |> String.fromFloat


toggleIsAcquired : Material -> Material
toggleIsAcquired material =
    { material | isAcquired = not material.isAcquired }


setName : String -> Material -> Material
setName name material =
    { material | name = name }


setCost : String -> Material -> Material
setCost cost material =
    { material | cost = Maybe.withDefault 0 (String.toFloat cost) }
