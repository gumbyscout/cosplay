module Page exposing (Page)

import Element exposing (Element)


type alias ActionButton msg =
    ( String, String, Maybe msg )


type alias Page msg =
    { title : String
    , content : Element msg
    , heading : Element msg
    , actions : List (ActionButton msg)
    }
