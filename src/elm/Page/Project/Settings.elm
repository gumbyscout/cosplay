module Page.Project.Settings exposing (view)

import Element exposing (Element, column, text)


view : Element msg
view =
    column [] [ text "SETTINGS" ]
