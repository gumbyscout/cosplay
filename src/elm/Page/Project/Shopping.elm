module Page.Project.Shopping exposing (view)

import Element exposing (Element, column, text)


view : Element msg
view =
    column [] [ text "SHOPPING" ]
