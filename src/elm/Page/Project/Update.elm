module Page.Project.Update exposing (Msg(..), changeRouteTo, update)

import Browser.Navigation as Nav
import Json.Decode as D
import Page.Project.Model exposing (Model)
import Project exposing (Project, ProjectID)
import ProjectTask exposing (Task, TaskID)
import RemoteData exposing (RemoteData(..))
import Route exposing (ProjectRoute)
import Storage exposing (Data, createTask)


type Msg
    = GotProject (Data Project)
    | GotTasks (Data (List Task))
    | SaveTask Task
    | CreateTask ProjectID
    | CreatedTask Task
    | CreateMaterial TaskID
    | DeleteTask Task
    | EditProject Project
    | SavedProject ProjectID D.Value
    | DeleteProject Project
    | DeletedProject ProjectID
    | NoOp


changeRouteTo : Nav.Key -> ProjectRoute -> ProjectID -> Model -> ( Model, Cmd Msg )
changeRouteTo navKey subRoute projectID model =
    let
        ( newModel, cmd ) =
            ( { model
                | route = subRoute
                , projectID = projectID
                , navKey = Just navKey
              }
            , if model.projectID /= projectID then
                Storage.getProject projectID

              else
                Cmd.none
            )
    in
    case subRoute of
        Route.Tasks ->
            case model.tasks of
                NotAsked ->
                    ( newModel, Cmd.batch [ cmd, Storage.getTasks projectID ] )

                _ ->
                    ( newModel, cmd )

        _ ->
            ( newModel, cmd )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotProject project ->
            ( { model | project = project }, Cmd.none )

        GotTasks tasks ->
            ( { model | tasks = tasks }, Cmd.none )

        DeleteProject project ->
            -- TODO: We should show a dialog or something to confirm delete
            ( model, Storage.deleteProject project )

        DeletedProject _ ->
            ( model
            , Maybe.map (\navKey -> Nav.pushUrl navKey (Route.toUrl Route.Projects)) model.navKey
                |> Maybe.withDefault Cmd.none
            )

        EditProject project ->
            ( { model | project = RemoteData.Success project }, Storage.saveProject project )

        SavedProject id meta ->
            let
                project =
                    if model.projectID == id then
                        RemoteData.map (\p -> { p | meta = meta }) model.project

                    else
                        model.project
            in
            ( { model | project = project }, Cmd.none )

        CreateTask projectID ->
            ( model, createTask projectID )

        CreatedTask task ->
            ( { model | tasks = RemoteData.map (\t -> t ++ [ task ]) model.tasks }, Cmd.none )

        SaveTask task ->
            ( { model
                | tasks =
                    RemoteData.map
                        (List.map
                            (\t ->
                                if t.id == task.id then
                                    task

                                else
                                    t
                            )
                        )
                        model.tasks
              }
            , Cmd.none
            )

        _ ->
            ( model, Cmd.none )
