module Page.Project.Model exposing (Model, initial)

import Browser.Navigation as Nav
import Project exposing (Project, ProjectID)
import ProjectTask exposing (Task, TaskID)
import RemoteData exposing (RemoteData)
import Route exposing (ProjectRoute)
import Storage exposing (Data)


initial : Model
initial =
    { route = Route.Tasks
    , projectID = ""
    , navKey = Nothing
    , tasks = RemoteData.NotAsked
    , project = RemoteData.NotAsked
    , selectedTask = Nothing
    }


type alias Model =
    { route : ProjectRoute
    , projectID : ProjectID
    , navKey : Maybe Nav.Key
    , tasks : Data (List Task)
    , project : Data Project
    , selectedTask : Maybe TaskID
    }
