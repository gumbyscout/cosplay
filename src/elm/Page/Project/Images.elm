module Page.Project.Images exposing (view)

import Element exposing (Element, column, text)


view : Element msg
view =
    column [] [ text "IMAGES" ]
