module Page.Project.Tasks exposing (view)

import Element exposing (Element, column, fill, link, row, spacing, text, width)
import Element.Input as Input
import Json.Decode as D
import Page.Project.Model exposing (Model)
import Page.Project.Update exposing (Msg(..))
import Project exposing (ProjectID)
import ProjectTask exposing (Task, TaskID, emptyTask, toggleIsDone)
import RemoteData exposing (RemoteData(..))
import Route
import Theme exposing (rhythm)


taskItem : Bool -> Task -> Element Msg
taskItem isSelected task =
    row []
        [ Input.checkbox
            []
            { checked = task.isDone
            , onChange = always (SaveTask <| toggleIsDone task)
            , icon = Input.defaultCheckbox
            , label =
                Input.labelRight [] <|
                    link []
                        { url = Route.toUrl <| Route.Project (Route.Task task.id) task.projectID
                        , label = text task.name
                        }
            }
        ]


taskTree : Maybe TaskID -> List Task -> Element Msg
taskTree selectedTask tasks =
    let
        isSelected task =
            Maybe.map (\a -> task.id == a) selectedTask
                |> Maybe.withDefault False
    in
    column []
        (List.map (\t -> taskItem (isSelected t) t) tasks)


actionBar : Model -> Element Msg
actionBar model =
    let
        newTaskBTN =
            Input.button []
                { onPress = Just (CreateTask <| model.projectID)
                , label = text "New Task"
                }
    in
    case model.selectedTask of
        Just id ->
            let
                task =
                    emptyTask
            in
            row []
                [ row []
                    [ newTaskBTN
                    , Input.button [] { onPress = Just (CreateMaterial id), label = text "New Material" }
                    ]
                , row []
                    [ Input.button [] { onPress = Just (DeleteTask task), label = text "Delete Task" } ]
                ]

        Nothing ->
            newTaskBTN


content : Model -> Element Msg
content model =
    let
        task =
            Nothing
    in
    case task of
        Just t ->
            -- Task.view model projectID t
            column [] [ text "TASK VIEW" ]

        Nothing ->
            -- TasksSummary.view model tasks
            column [] [ text "TASK SUMMARY" ]


view : Model -> Element Msg
view model =
    column [ width fill ]
        [ actionBar model
        , row [ width fill, spacing rhythm ]
            [ column []
                [ case model.tasks of
                    Success tasks ->
                        taskTree model.selectedTask tasks

                    Loading ->
                        text "Loading"

                    Failure e ->
                        text <| D.errorToString e

                    NotAsked ->
                        text "Not Asked"
                ]
            , column [ width fill ] [ content model ]
            ]
        ]
