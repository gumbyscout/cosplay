module Page.NotFound exposing (view)

import Browser exposing (Document)
import Element exposing (el, padding, text)
import Element.Font as Font
import Element.Region as Region
import Page exposing (Page)
import Theme exposing (rhythm, scaled)


view : Page msg
view =
    { title = "404 Not Found"
    , heading = text "Page not found"
    , content =
        el [ Region.heading 1, Font.size (scaled 4), padding rhythm ] <| text "404 Page Not Found"
    , actions = []
    }
