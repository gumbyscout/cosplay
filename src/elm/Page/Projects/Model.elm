module Page.Projects.Model exposing (Model, Projects, initial)

import Browser.Navigation as Nav
import Project exposing (Project)
import RemoteData exposing (RemoteData)
import Storage exposing (Data)


initial : List Project -> Model
initial projects =
    { projects = RemoteData.Success projects, navKey = Nothing }


type alias Projects =
    Data (List Project)


type alias Model =
    { projects : Projects
    , navKey : Maybe Nav.Key
    }
