module Page.Projects exposing (Msg, init, subscriptions, update, view)

import Browser exposing (Document)
import Browser.Navigation as Nav
import Components.FAB as FAB
import Components.Icon as Icon
import Dict
import Element
    exposing
        ( Element
        , alignBottom
        , alignRight
        , alignTop
        , classifyDevice
        , clip
        , column
        , el
        , fill
        , fillPortion
        , height
        , image
        , inFront
        , link
        , padding
        , paddingXY
        , row
        , scrollbarY
        , spacing
        , text
        , width
        , wrappedRow
        )
import Element.Background as Background
import Element.Font as Font
import Element.Input exposing (button)
import Element.Region as Region
import Image exposing (getThumbnail)
import Page exposing (Page)
import Page.Projects.Model exposing (Model, Projects)
import Project exposing (Project, ProjectID, remainingTimeInDays)
import RemoteData exposing (RemoteData(..))
import Route
import Storage exposing (Action)
import Theme exposing (color, rhythm, scaled)
import Time exposing (Posix)
import Utils exposing (cyAttribute, monthToNumString)


type Msg
    = GotProjects Projects
    | CreateProject
    | CreatedProject ProjectID
    | NoOp


init : Nav.Key -> Model -> ( Model, Cmd Msg )
init navKey model =
    ( { model | navKey = Just navKey }, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotProjects projects ->
            ( { model | projects = projects }, Cmd.none )

        CreateProject ->
            ( model, Storage.createProject )

        CreatedProject id ->
            ( model, Maybe.map (\n -> Nav.pushUrl n (Route.toUrl <| Route.Project Route.Tasks id)) model.navKey |> Maybe.withDefault Cmd.none )

        NoOp ->
            ( model, Cmd.none )


handleAction : Action -> Msg
handleAction a =
    case a of
        Storage.GotProjects projects ->
            GotProjects (RemoteData.Success projects)

        Storage.CreatedProject id ->
            CreatedProject id

        _ ->
            NoOp


subscriptions : Model -> Sub Msg
subscriptions model =
    Storage.gotAction
        (RemoteData.withDefault NoOp << RemoteData.map handleAction)


formatDate : Posix -> String
formatDate posix =
    let
        year =
            Time.toYear Time.utc posix
                |> String.fromInt

        day =
            Time.toDay Time.utc posix
                |> String.fromInt

        month =
            Time.toMonth Time.utc posix
                |> monthToNumString
    in
    year ++ "/" ++ month ++ "/" ++ day


item : Project -> Element Msg
item project =
    link
        [ cyAttribute "project-item"
        , Background.color color.material
        , padding rhythm
        , width fill
        ]
        { url = Route.toUrl <| Route.Project Route.Tasks project.id
        , label =
            row [ width fill ]
                [ column [ spacing rhythm, width fill, alignTop ]
                    [ row [ spacing rhythm, cyAttribute "project-item__name" ]
                        [ el [ Font.size <| scaled 2, Font.underline ] <| text project.name ]
                    , row []
                        [ text
                            ("Due Date (UTC): "
                                ++ (Maybe.map formatDate project.targetDate |> Maybe.withDefault "N/A")
                            )
                        ]
                    , row []
                        [ text "Estimated Time: TODO" ]
                    , row []
                        [ text "Tracked Time: TODO" ]
                    ]
                , column []
                    [ image [] { src = getThumbnail project.media, description = "" } ]
                ]
        }


createProjectBtn : Element Msg
createProjectBtn =
    el
        [ alignBottom
        , alignRight
        , padding rhythm
        , cyAttribute "create-project-btn"
        ]
        (FAB.view (Just CreateProject) "plus")


projectsView : Model -> Element Msg
projectsView model =
    column
        [ width fill
        , height fill
        , inFront createProjectBtn
        , scrollbarY
        , padding rhythm
        , spacing rhythm
        ]
    <|
        case model.projects of
            Success projects ->
                List.map item projects

            _ ->
                [ Element.none ]


view : Model -> Page Msg
view model =
    { title = "Projects"
    , content = projectsView model
    , actions = [ ( "sliders-h", "filter-btn", Nothing ) ]
    , heading = text "Projects"
    }
