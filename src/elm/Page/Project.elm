module Page.Project exposing (subscriptions, view)

import Element exposing (Element, column, el, fill, height, html, link, padding, paddingEach, row, scrollbarX, spacing, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Page exposing (Page)
import Page.Project.Images as Images
import Page.Project.Model exposing (Model)
import Page.Project.Settings as Settings
import Page.Project.Shopping as Shopping
import Page.Project.Tasks as Tasks
import Page.Project.Update exposing (Msg(..))
import Project exposing (Project, ProjectID, setName)
import ProjectTask exposing (Task)
import RemoteData exposing (RemoteData(..))
import Route
import Storage exposing (Action)
import Theme exposing (color, rhythm, scaled)
import Utils exposing (cyAttribute)


type alias Tab =
    { label : String
    , url : String
    }


tab : Bool -> Tab -> Element Msg
tab selected t =
    let
        bar =
            4

        attrs =
            if selected then
                [ Border.widthEach { bottom = bar, left = 0, right = 0, top = 0 }
                , Border.color color.secondary
                , paddingEach { bottom = rhythm - bar, left = rhythm, right = rhythm, top = rhythm }
                ]

            else
                [ padding rhythm ]
    in
    link attrs { url = t.url, label = text t.label }


generateTabs : Model -> Int -> List (Element Msg)
generateTabs model currentTab =
    let
        ts =
            [ Tab "Tasks" (Route.toUrl <| Route.Project Route.Tasks model.projectID)
            , Tab "Images" (Route.toUrl <| Route.Project Route.Images model.projectID)
            , Tab "Shopping" (Route.toUrl <| Route.Project Route.Shopping model.projectID)
            , Tab "Settings" (Route.toUrl <| Route.Project Route.Settings model.projectID)
            ]
    in
    List.indexedMap (\index t -> tab (index == currentTab) t) ts


tabs : Model -> Int -> Element Msg
tabs model currentTab =
    row
        [ Background.color color.primaryLight
        , Font.color color.primaryLightText
        , width fill
        , Region.navigation
        , scrollbarX
        ]
    <|
        generateTabs model currentTab


pageTitle : Model -> String
pageTitle model =
    let
        subTitle =
            case model.route of
                Route.Tasks ->
                    "Tasks"

                Route.Task tid ->
                    -- TODO: Get the task name
                    "Task"

                Route.Images ->
                    "Images"

                Route.Settings ->
                    "Settings"

                Route.Shopping ->
                    "Shopping"
    in
    RemoteData.map (\p -> p.name ++ " - " ++ subTitle) model.project
        |> RemoteData.withDefault "Project"


view : Model -> Page Msg
view model =
    let
        ( currentTab, currentView ) =
            case model.route of
                Route.Tasks ->
                    ( 0, Tasks.view model )

                Route.Task _ ->
                    ( 0, Tasks.view model )

                Route.Images ->
                    ( 1, Images.view )

                Route.Shopping ->
                    ( 2, Shopping.view )

                Route.Settings ->
                    ( 3, Settings.view )
    in
    { title = pageTitle model
    , content =
        column [ width fill, height fill ]
            [ tabs model currentTab
            , currentView
            ]
    , heading =
        case model.project of
            Success project ->
                Input.text
                    [ padding 0
                    , Border.width 0
                    , Background.color color.primary
                    , cyAttribute "project-name"
                    ]
                    { onChange = EditProject << setName project
                    , text = project.name
                    , label = Input.labelHidden "project title"
                    , placeholder = Just (Input.placeholder [ Font.color color.primaryText ] (text "Title"))
                    }

            _ ->
                text "NO PROJECT"
    , actions =
        [ ( "trash"
          , "delete-project-btn"
          , Maybe.map DeleteProject (RemoteData.toMaybe model.project)
          )
        ]
    }


handleAction : Action -> Msg
handleAction a =
    case a of
        Storage.GotProject project ->
            GotProject (RemoteData.Success project)

        Storage.GotTasks tasks ->
            GotTasks (RemoteData.Success tasks)

        Storage.DeletedProject projectID ->
            DeletedProject projectID

        Storage.SavedProject { id, meta } ->
            SavedProject id meta

        Storage.CreatedTask task ->
            CreatedTask task

        _ ->
            NoOp


handleError : e -> Msg
handleError e =
    -- let
    --     _ =
    --         Debug.log "error" e
    -- in
    NoOp


subscriptions : Model -> Sub Msg
subscriptions model =
    Storage.gotAction
        (RemoteData.withDefault NoOp << RemoteData.mapError handleError << RemoteData.map handleAction)
