module Route exposing (ProjectRoute(..), Route(..), fromUrl, toUrl)

import Project exposing (ProjectID)
import ProjectTask exposing (TaskID)
import Url exposing (Url)
import Url.Parser exposing ((</>), Parser, map, oneOf, parse, s, string, top)


type ProjectRoute
    = Tasks
    | Task TaskID
    | Settings
    | Images
    | Shopping


type Route
    = Projects
    | NewProject
    | NotFound
    | Project ProjectRoute ProjectID
    | Statistics
    | AppSettings


projectRoot : Parser (String -> a) a
projectRoot =
    s "projects" </> string


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map Projects top
        , map (Project Tasks) projectRoot
        , map (Project Tasks) (projectRoot </> s "tasks")
        , map (Project << Task) (projectRoot </> s "tasks" </> string)
        , map (Project Settings) (projectRoot </> s "settings")
        , map (Project Shopping) (projectRoot </> s "shopping")
        , map (Project Images) (projectRoot </> s "images")
        , map Projects (s "projects")
        , map NewProject (s "new-project")
        ]


fromUrl : Url -> Route
fromUrl location =
    case parse matchers location of
        Just route ->
            route

        Nothing ->
            NotFound


rootURL : String
rootURL =
    ""


homeURL : String
homeURL =
    rootURL


projectsURL : String
projectsURL =
    rootURL ++ "/projects"


statsURL : String
statsURL =
    rootURL ++ "/stats"


appSettingsURL : String
appSettingsURL =
    rootURL ++ "/settings"


newProjectURL : String
newProjectURL =
    rootURL ++ "/new-project"


tasksURL : ProjectID -> String
tasksURL projectID =
    projectURL projectID ++ "/tasks"


imagesURL : ProjectID -> String
imagesURL projectID =
    projectURL projectID ++ "/images"


shoppingURL : ProjectID -> String
shoppingURL projectID =
    projectURL projectID ++ "/shopping"


settingsURL : ProjectID -> String
settingsURL projectID =
    projectURL projectID ++ "/settings"


projectURL : ProjectID -> String
projectURL projectID =
    projectsURL ++ "/" ++ projectID


taskURL : ProjectID -> TaskID -> String
taskURL projectID taskID =
    tasksURL projectID ++ "/" ++ taskID


toUrl : Route -> String
toUrl route =
    case route of
        Projects ->
            projectsURL

        NewProject ->
            newProjectURL

        Project subRoute projectID ->
            case subRoute of
                Tasks ->
                    tasksURL projectID

                Task taskID ->
                    taskURL projectID taskID

                Settings ->
                    settingsURL projectID

                Images ->
                    imagesURL projectID

                Shopping ->
                    shoppingURL projectID

        Statistics ->
            statsURL

        AppSettings ->
            appSettingsURL

        NotFound ->
            "404"
