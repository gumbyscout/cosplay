module Components.Toast exposing (view)

import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import Models exposing (Model)
import Msgs exposing (Msg)



-- toastView : Toast.NotificationState -> String -> Html Msg
-- toastView state msg =
--     div [ class "notification is-primary" ]
--         [ text msg
--         ]


view : Model -> Html Msg
view model =
    -- div [ class "toasts" ] <| Toast.views model.toast model.time toastView
    div [] []
