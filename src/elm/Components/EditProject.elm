module Components.EditProject exposing (view)

import DatePicker
import Html exposing (Html, div, input, label, text)
import Html.Attributes exposing (class, type_, value)
import Html.Events exposing (onInput)
import Models
import Msgs exposing (..)
import Project exposing (Project)


view : Models.Model -> Project -> Html Msg
view model project =
    div [ class "o-grid o-grid--wrap o-grid__cell--no-gutter" ]
        [ div [ class "o-grid__cell o-grid__cell--width-100" ]
            [ div [ class "o-form-element" ]
                [ label [ class "c-label" ] [ text "Name" ]
                , input
                    [ class "c-field"
                    , value project.name
                    , onInput (\name -> UpdateEditProject { project | name = name })
                    ]
                    []
                ]
            ]
        , div [ class "o-grid__cell o-grid__cell--width-100" ]
            [ div [ class "o-form-element" ]
                [ label [ class "c-label" ] [ text "Target Date" ]

                -- , DatePicker.view project.target DatePicker.defaultSettings model.datePicker |> Html.map (\cmd -> UpdateEditProjectDate ( cmd, project ))
                ]
            ]
        , div [ class "o-grid__cell o-grid__cell--width-100" ]
            [ div [ class "o-form-element" ]
                [ label [ class "c-label" ] [ text "Budget" ]
                , input
                    [ class "c-field"
                    , type_ "number"
                    , value (String.fromFloat project.budget)
                    , onInput (\num -> UpdateEditProject { project | budget = Maybe.withDefault 0 <| String.toFloat num })
                    ]
                    []
                ]
            ]
        ]
