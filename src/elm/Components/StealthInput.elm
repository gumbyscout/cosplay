module Components.StealthInput exposing (Msg, view)

import Html exposing (Html, div, form, input, text, textarea)
import Html.Attributes exposing (class, hidden, id, name, type_, value)
import Html.Events exposing (onBlur, onClick, onInput, onSubmit)


type Msg
    = UpdateStealthInput (Maybe ( String, String )) (Maybe msg)
    | FocusStealthInput String (Maybe ( String, String ))


getInputID : String -> String
getInputID name =
    "stealth-input-" ++ name


displayMode : Bool -> String -> String -> Html msg
displayMode isHidden fieldName fieldValue =
    div
        [ onClick <| FocusStealthInput (getInputID fieldName) (Just ( fieldName, fieldValue ))
        , hidden isHidden
        ]
        [ text fieldValue ]


editMode : Bool -> String -> String -> String -> String -> (String -> msg) -> Html msg
editMode isHidden inputType stealthValue fieldName fieldValue msg =
    let
        save =
            UpdateStealthInput Nothing (Just (msg stealthValue))

        update =
            \val -> UpdateStealthInput (Just ( fieldName, val )) Nothing

        attrs =
            [ value stealthValue
            , name fieldName
            , onBlur save
            , onInput update
            , id <| getInputID fieldName
            ]

        ele =
            case inputType of
                "textarea" ->
                    textarea attrs

                _ ->
                    input (type_ inputType :: attrs)
    in
    form [ onSubmit save, hidden isHidden ] [ ele [] ]


view : model -> String -> String -> String -> (String -> msg) -> Html msg
view model inputType fieldName fieldValue updateMsg =
    let
        ( _, stealthValue ) =
            Maybe.withDefault ( "", fieldValue ) model.stealthInput

        hideEdit =
            case model.stealthInput of
                Just ( n, value ) ->
                    n == fieldName

                Nothing ->
                    False
    in
    div [ class "stealth-input" ]
        [ editMode (not hideEdit) inputType stealthValue fieldName fieldValue updateMsg
        , displayMode hideEdit fieldName fieldValue
        ]
