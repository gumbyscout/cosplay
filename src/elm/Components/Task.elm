module Components.Task exposing (view)

import Components.MaterialsTable as MaterialsTable
import Components.StealthInput as StealthInput
import Dict
import Html exposing (Html, div, p, text)
import Html.Attributes exposing (class)
import Models exposing (Material, Model)
import Msgs exposing (..)
import Project exposing (ProjectID)
import ProjectTask exposing (Task, setDescription, setName)
import RemoteData
import Utils.Materials exposing (calcBudget, calcCurrentCost)



-- TODO: Get materials from state.


statistics : Task -> List Material -> Html Msg
statistics task materials =
    div []
        [ div [] [ text <| "Estimated budget: $" ++ calcBudget materials ]
        , div [] [ text <| "Cost so far: $" ++ calcCurrentCost materials ]
        ]


nameInput : Model -> ProjectID -> Task -> Html Msg
nameInput model projectID task =
    StealthInput.view model
        "text"
        "task-name"
        task.name
        (SaveTask << (\a -> setName a task))


descriptionInput : Model -> ProjectID -> Task -> Html Msg
descriptionInput model projectID task =
    let
        description =
            case String.isEmpty task.description of
                False ->
                    task.description

                True ->
                    "Click here to add a description"
    in
    StealthInput.view model
        "textarea"
        "task-description"
        description
        (SaveTask << (\a -> setDescription a task))


view : Model -> ProjectID -> Task -> Html Msg
view model projectID task =
    let
        materials =
            model.materials
                |> RemoteData.withDefault Dict.empty
                |> Dict.filter (\t v -> Tuple.first t == task.id)
                |> Dict.values
    in
    div []
        [ p [ class "title is-2 task__name" ] [ nameInput model projectID task ]
        , div [ class "task__section" ]
            [ p [ class "title is-3" ] [ text "Description" ]
            , descriptionInput model projectID task
            ]
        , div [ class "task__section" ]
            [ p [ class "title is-3" ] [ text "Statistics" ]
            , statistics task materials
            ]
        , div [ class "task__section" ]
            [ p [ class "title is-3" ] [ text "Materials" ]
            , MaterialsTable.view model projectID task.id materials
            ]
        ]
