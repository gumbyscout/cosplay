module Components.TasksSummary exposing (view)

import Html exposing (Html, div, h2, text)
import Html.Attributes exposing (class)
import Models exposing (Material, Model)
import Msgs exposing (Msg)
import ProjectTask exposing (Task)
import Utils.Materials exposing (calcBudget, calcCurrentCost)



-- TODO: Get all materials from state.


statistics : List Material -> Html Msg
statistics materials =
    div []
        [ div [] [ text <| "Estimated budget: $" ++ calcBudget materials ]
        , div [] [ text <| "Cost so far: $" ++ calcCurrentCost materials ]
        ]


view : Model -> List Task -> Html Msg
view model tasks =
    let
        materials =
            []

        completedTasks =
            tasks
                |> List.filter .isDone
                |> List.length
    in
    div []
        [ h2 [ class "title is-2 task__name" ] [ text "Tasks Summary" ]
        , div [] [ text <| "Completed tasks: " ++ String.fromInt completedTasks ]
        , statistics materials
        ]
