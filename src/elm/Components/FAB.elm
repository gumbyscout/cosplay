module Components.FAB exposing (view)

import Components.Icon as Icon
import Element exposing (Element, centerX, el, height, padding, px, rgba, spacing, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Input exposing (button)
import Theme exposing (color, rhythm)


view : Maybe msg -> String -> Element msg
view onPress icon =
    button
        [ Border.rounded (24 + 16 * 2)
        , padding 24
        , Background.color color.secondary
        , Border.shadow { offset = ( 0, 2 ), size = 2, blur = 0, color = rgba 0 0 0 0.1 }
        ]
        { onPress = onPress
        , label =
            Icon.view icon
        }
