module Components.MaterialsTable exposing (view)

import Components.StealthInput as StealthInput
import Html exposing (Html, button, div, input, span, text)
import Html.Attributes exposing (checked, class, type_)
import Html.Events exposing (onClick)
import Models exposing (Material, Model)
import Msgs exposing (..)
import Project exposing (ProjectID)
import ProjectTask exposing (TaskID)
import Utils.Materials exposing (setCost, setName, toggleIsAcquired)


nameInput : Model -> ProjectID -> TaskID -> Material -> Html Msg
nameInput model projectID taskID material =
    StealthInput.view model
        "text"
        (material.id ++ "-material-name")
        material.name
        (SaveMaterial << (\a -> setName a material))


costInput : Model -> ProjectID -> TaskID -> Material -> Html Msg
costInput model projectID taskID material =
    StealthInput.view model
        "text"
        (material.id ++ "-material-cost")
        (String.fromFloat material.cost)
        (SaveMaterial << (\a -> setCost a material))


materialItem : Model -> ProjectID -> TaskID -> Material -> Html Msg
materialItem model projectID taskID material =
    div [ class "materials-table-item" ]
        [ span [ class "materials-table-item__checkbox" ]
            [ input
                [ type_ "checkbox"
                , checked material.isAcquired
                , onClick <| SaveMaterial <| toggleIsAcquired material
                ]
                []
            ]
        , div [ class "materials-table-item__label" ]
            [ span [ class "materials-table-item__name" ]
                [ nameInput model projectID taskID material ]
            , span [ class "materials-table-item__cost" ]
                [ costInput model projectID taskID material ]
            , button [ class "materials-table-item__remove-btn", onClick <| DeleteMaterial material.id ] [ text "X" ]
            ]
        ]


view : Model -> ProjectID -> TaskID -> List Material -> Html Msg
view model projectID taskID materials =
    div [ class "materials-table" ] (List.map (materialItem model projectID taskID) materials)
