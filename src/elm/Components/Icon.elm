module Components.Icon exposing (view)

import Element exposing (Element, el, height, html, px, width)
import Svg exposing (svg, use)
import Svg.Attributes exposing (xlinkHref)
import Theme exposing (rhythm)


view : String -> Element msg
view name =
    el
        [ height <| px rhythm
        , width <| px rhythm
        ]
    <|
        html <|
            svg
                []
                [ use [ xlinkHref ("/icons/" ++ name ++ ".svg#" ++ name) ] [] ]
