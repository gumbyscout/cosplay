module Flags exposing (Flags, decode)

import Json.Decode as D exposing (Decoder, Value)
import Json.Decode.Pipeline exposing (optional, required)
import Project exposing (Project)


type alias Flags =
    { window : Window
    , projects : List Project
    }


type alias Window =
    { width : Int, height : Int }


decodeWindow : Decoder Window
decodeWindow =
    D.succeed Window
        |> required "width" D.int
        |> required "height" D.int


decode : Decoder Flags
decode =
    D.succeed Flags
        |> required "window" decodeWindow
        |> optional "projects" (D.list Project.decode) []
