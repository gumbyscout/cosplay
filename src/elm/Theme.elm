module Theme exposing (color, halfRhythm, hex, rhythm, scaled)

import Element exposing (modular, rgb255)
import Hex


hexSegment : Int -> Int -> String -> Int
hexSegment s e str =
    String.slice s e str
        |> String.toLower
        |> Hex.fromString
        |> Result.withDefault 0


hex : String -> Element.Color
hex h =
    if String.length h /= 7 then
        rgb255 0 0 0

    else
        let
            r =
                hexSegment 1 3 h

            g =
                hexSegment 3 5 h

            b =
                hexSegment 5 7 h
        in
        rgb255 r g b


type alias Color =
    { primary : Element.Color
    , secondary : Element.Color
    , primaryDark : Element.Color
    , secondaryDark : Element.Color
    , primaryLight : Element.Color
    , secondaryLight : Element.Color
    , primaryText : Element.Color
    , secondaryText : Element.Color
    , primaryDarkText : Element.Color
    , secondaryDarkText : Element.Color
    , primaryLightText : Element.Color
    , secondaryLightText : Element.Color
    , background : Element.Color
    , material : Element.Color
    }


rhythm : number
rhythm =
    16


halfRhythm : number
halfRhythm =
    8


scaled : Int -> Int
scaled =
    round << modular rhythm 1.25


white : Element.Color
white =
    hex "#ffffff"


black : Element.Color
black =
    hex "#000000"


{-| See <https://material.io/tools/color/#!/?view.left=0&view.right=0&primary.color=827717&secondary.color=C6FF00> for theme
-}
color : Color
color =
    { primary = hex "#827717"
    , primaryDark = hex "#524c00"
    , primaryLight = hex "#b4a647"
    , primaryText = white
    , primaryDarkText = white
    , primaryLightText = black
    , secondary = hex "#c6ff00"
    , secondaryLight = hex "#fdff58"
    , secondaryDark = hex "#90cc00"
    , secondaryText = black
    , secondaryLightText = black
    , secondaryDarkText = black
    , background = hex "#e1e2e1"
    , material = hex "#F5F5F6"
    }
