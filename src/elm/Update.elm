module Update exposing (Msg(..), changeRouteTo, update)

import Browser
import Browser.Navigation as Nav
import Model exposing (Model, closeDrawer, updateProjectModel, updateProjectsModel)
import Page.Project as Project
import Page.Project.Update as ProjectUpdate
import Page.Projects as Projects
import Route exposing (Route)
import Url exposing (Url)


type Msg
    = ChangedRoute (Maybe Route)
    | ChangedUrl Url
    | ClickedLink Browser.UrlRequest
    | GotProjectsMsg Projects.Msg
    | GotProjectMsg ProjectUpdate.Msg
    | WindowResized Int Int
    | ToggleDrawer


changeRouteTo : Route -> Model -> ( Model, Cmd Msg )
changeRouteTo route m =
    let
        session =
            closeDrawer model

        model =
            { m | route = route }
    in
    case route of
        Route.Projects ->
            updateSub GotProjectsMsg (updateProjectsModel model) (Projects.init model.navKey m.projectsModel)

        Route.Project subRoute projectID ->
            ProjectUpdate.changeRouteTo model.navKey subRoute projectID model.projectModel
                |> updateSub GotProjectMsg (updateProjectModel model)

        _ ->
            ( model, Cmd.none )


updateSub : (subMsg -> Msg) -> (subModel -> Model) -> ( subModel, Cmd subMsg ) -> ( Model, Cmd Msg )
updateSub toMsg updater ( subModel, subMsg ) =
    ( updater subModel, Cmd.map toMsg subMsg )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotProjectsMsg subMsg ->
            Projects.update subMsg model.projectsModel
                |> updateSub GotProjectsMsg (updateProjectsModel model)

        GotProjectMsg subMsg ->
            ProjectUpdate.update subMsg model.projectModel
                |> updateSub GotProjectMsg (updateProjectModel model)

        ClickedLink urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.navKey (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        ChangedUrl url ->
            changeRouteTo (Route.fromUrl url) model

        WindowResized width height ->
            ( { model | window = { width = width, height = height } }, Cmd.none )

        ToggleDrawer ->
            ( { model | drawerIsOpen = not model.drawerIsOpen }, Cmd.none )

        _ ->
            ( model, Cmd.none )
