import "babel-polyfill";

import { mockData } from "./re/utils.re";

(async () => {
  const { Elm } = await import("./elm/Main.elm");

  const [projects, tasks] = mockData();

  console.warn("data", projects, tasks);

  Elm.Main.init({
    node: document.getElementById("root"),
    flags: {
      window: {
        width: window.innerWidth,
        height: window.innerHeight
      },
      projects,
      tasks
    }
  });
})();
