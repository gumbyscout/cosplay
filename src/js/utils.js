import jdenticon from "jdenticon";

// TODO: Blob urls will infinitely populate into memory without manual cleanup
// This function still exists in js land, because it's annoying to try to figure
// out how to write it in reason.
export const generateThumbnail = name => {
  const svg = jdenticon.toSvg(name, 100);
  const blob = new Blob([svg], { type: "image/svg+xml" });
  return URL.createObjectURL(blob);
};
