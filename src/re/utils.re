[@bs.module "shortid"] external generateID: unit => string = "generate";
[@bs.module "../js/utils"]
external generateThumbnail: string => string = "generateThumbnail";

type _moment;
[@bs.module] external moment: unit => _moment = "moment";
[@bs.send] external valueOf: (_moment, unit) => int = "valueOf";
/* These two methods mutate a moment, which reason doesn't catch */
[@bs.send] external add: (_moment, int, string) => _moment = "add";
[@bs.send] external subtract: (_moment, int, string) => _moment = "subtract";

let millis: _moment => int = _m => _m->valueOf();

/* You need this annotation for records to serialize as js objects.
 * This also means, these no longer behave as records so
 * it's kind of a pain to access their properties or create them.
 * See https://bucklescript.github.io/docs/en/object.html#record-mode
 */
[@bs.deriving abstract]
type image = {
  name: string,
  url: string,
};

[@bs.deriving abstract]
type task = {
  id: string,
  projectID: string,
  name: string,
  isDone: bool,
  description: string,
};

[@bs.deriving abstract]
type project = {
  id: string,
  name: string,
  createdDate: int,
  [@bs.optional]
  targetDate: int,
  budget: float,
  /* Array maps directly to js arrays. */
  media: array(image),
};

let projectNames = [|"Tactician", "Harvest Goddess", "Nurse Joy"|];

let newProject: string => project =
  name => {
    let thumbnail = image(~name="thumbnail", ~url=generateThumbnail(name));
    project(
      ~id=generateID(),
      ~name,
      ~createdDate=moment()->subtract(7, "days")->millis,
      ~budget=0.0,
      ~media=[|thumbnail|],
      ~targetDate=moment()->add(7, "days")->millis,
      (),
    );
  };

let newTask: (string, string) => task =
  (projectID, name) =>
    task(~id=generateID(), ~projectID, ~name, ~isDone=false, ~description="");

/* Generate mock data for the application */
let mockData: unit => (array(project), array(task)) =
  () => {
    let projects = Array.map(newProject, projectNames);
    let tasks =
      Array.map(project => newTask(project->idGet, "New Task"), projects);
    (projects, tasks);
  };
