module ThemeTests exposing (hexTest)

import Element exposing (rgb255)
import Expect
import Test exposing (..)
import Theme exposing (hex)


hexTest =
    describe "hex"
        [ test "it converts white properly" <|
            \_ ->
                hex "#FFFFFF"
                    |> Expect.equal (rgb255 255 255 255)
        , test "it returns black on error" <|
            \_ ->
                hex "badhfldkaj"
                    |> Expect.equal (rgb255 0 0 0)
        , test "it converts lime green" <|
            \_ ->
                hex "#c6ff00"
                    |> Expect.equal (rgb255 198 255 0)
        ]
